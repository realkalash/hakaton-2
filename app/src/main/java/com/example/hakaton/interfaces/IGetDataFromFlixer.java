package com.example.hakaton.interfaces;

import android.content.Context;

import com.example.hakaton.adapter.CurrrencyAdapter;

public interface IGetDataFromFlixer {
    void getData(CurrrencyAdapter currrencyAdapter, Context context);
}
