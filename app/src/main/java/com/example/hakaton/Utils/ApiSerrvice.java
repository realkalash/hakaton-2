package com.example.hakaton.Utils;

import com.example.hakaton.model.ModelCurrency;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiSerrvice {
    private static final String API = "http://data.fixer.io/";
    private static PrivateApi privateApi;

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);

    }

    public static Call<ModelCurrency> getData(String accessKey, String base, String date, String symbols) {

        if (!date.equals("")) {
           return privateApi.getExchangeRateWithDate(date,accessKey,base,symbols);

        } else {
            return privateApi.getExchangeRates(accessKey, base, symbols);
        }
    }


    public interface PrivateApi {
        @GET("api/latest")
        Call<ModelCurrency> getExchangeRates(@Query("access_key") String accessKey, @Query("base") String base, @Query("symbols") String symbols);


        @GET("api/{qdate}")
        Call<ModelCurrency> getExchangeRateWithDate(@Path("qdate") String qdate, @Query("access_key") String accessKey, @Query("base") String base, @Query("symbols") String symbols);
    }

}
