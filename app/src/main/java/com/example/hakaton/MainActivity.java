package com.example.hakaton;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hakaton.Utils.ApiSerrvice;
import com.example.hakaton.Utils.Utils;
import com.example.hakaton.adapter.CurrrencyAdapter;
import com.example.hakaton.interfaces.IGetDataFromFlixer;
import com.example.hakaton.model.ModelCurrency;
import com.example.hakaton.model.RatesModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements CurrrencyAdapter.OnItemClickListener, IGetDataFromFlixer, CurrrencyAdapter.LongClickListener {

    public static final String NAME_CURRENCY = "name_currency";
    public static final String DESCRIPTION_CURRENCY = "description_currency";
    public static final String ICON_CURRENCY = "icon_currency";
    public static final String CODE_CURRENCY = "code_currency";
    public static final String RATE_CURRENCY = "rate_currency";

    @BindView(R.id.am_currency_list)
    RecyclerView recyclerViewCurrency;

    Context context = this;
    Utils utils = new Utils();

    List<RatesModel> ratesModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CurrrencyAdapter adapter = new CurrrencyAdapter(ratesModelList, MainActivity.this,
                true, MainActivity.this, MainActivity.this);

        getData(adapter, context);


        ButterKnife.bind(this);


    }


    @Override
    public void OnItemClick(RatesModel ratesModel) {
        Intent intent = new Intent(this, CurrencyInfoActivity.class);
        intent.putExtra(NAME_CURRENCY, ratesModel.getCurrencyTitle());
        intent.putExtra(DESCRIPTION_CURRENCY, ratesModel.getCurrencyDescription());
        intent.putExtra(CODE_CURRENCY, ratesModel.getCurrencyCode());
        intent.putExtra(ICON_CURRENCY, ratesModel.getCurrencyIcon());
        intent.putExtra(RATE_CURRENCY, ratesModel.getCurrencyRate());
        startActivity(intent);
    }

    public void ErrorInternet(final CurrrencyAdapter adapterCurrencys) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Error")
                .setMessage("Error with internet. Check your connection and press <<Retry>>")
                .setCancelable(false)
                .setPositiveButton("retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getData(adapterCurrencys, context);
                    }
                })
                .setNegativeButton("exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void getData(final CurrrencyAdapter currrencyAdapter, final Context context) {

        final String filteredNamesCurrencies = utils.getNamesCurrencies(context);


        ApiSerrvice.getData("8346ccee731e19db37259397720b49ce", "EUR", "", filteredNamesCurrencies).enqueue(new Callback<ModelCurrency>() {
            @Override
            public void onResponse(Call<ModelCurrency> call, Response<ModelCurrency> response) {

                if (response.isSuccessful() && response.body() != null) {

                    ModelCurrency modelCurrency = response.body();

                    ratesModelList.add(new RatesModel("usd", "USD", getApplicationContext().getString(R.string.usdDes), modelCurrency.getRates().getuSD(), context, "usd"));
                    ratesModelList.add(new RatesModel("eur", "EUR", getApplicationContext().getString(R.string.eurDes), modelCurrency.getRates().geteUR(), context, "usd"));
                    ratesModelList.add(new RatesModel("gbp", "GBP", getApplicationContext().getString(R.string.gbpDes), modelCurrency.getRates().getGBP(), context, "usd"));
                    ratesModelList.add(new RatesModel("uah", "UAH", getApplicationContext().getString(R.string.uahDes), modelCurrency.getRates().getuAH(), context, "usd"));
                    ratesModelList.add(new RatesModel("pln", "PLN", getApplicationContext().getString(R.string.plnDes), modelCurrency.getRates().getpLN(), context, "usd"));


                    recyclerViewCurrency.setAdapter(currrencyAdapter);

                } else {
                    utils.ErrorInternet(currrencyAdapter, context);
                    if (utils.isOnClickRetry()) {
                        getData(currrencyAdapter, context);
                    } else finish();
                }
            }

            @Override
            public void onFailure(Call<ModelCurrency> call, Throwable t) {
                utils.ErrorInternet(currrencyAdapter, context);
                if (utils.isOnClickRetry()) {
                    getData(currrencyAdapter, context);
                } else finish();
            }
        });

    }

    @Override
    public void longItemClick(RatesModel ratesModel) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(NAME_CURRENCY, ratesModel.getCurrencyTitle());
        intent.putExtra(DESCRIPTION_CURRENCY, ratesModel.getCurrencyDescription());
        intent.putExtra(CODE_CURRENCY, ratesModel.getCurrencyCode());
        intent.putExtra(ICON_CURRENCY, ratesModel.getCurrencyIcon());
        intent.putExtra(RATE_CURRENCY, ratesModel.getCurrencyRate());
        startActivity(intent);
    }

}
