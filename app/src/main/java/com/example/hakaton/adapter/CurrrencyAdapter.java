package com.example.hakaton.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hakaton.R;
import com.example.hakaton.model.RatesModel;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrrencyAdapter extends RecyclerView.Adapter<CurrrencyAdapter.ViewHolder>{
    List<RatesModel> modelCurrencies;
    Context context;
    Boolean showRates;
    RatesModel ratesModel;
    private OnItemClickListener listener;
    private LongClickListener longItemClickListener;

    public CurrrencyAdapter(List<RatesModel> modelCurrencies, Context context, Boolean showRates,
                            OnItemClickListener listener, LongClickListener longItemClickListener) {

        this.modelCurrencies = modelCurrencies;
        this.context = context;
        this.showRates = showRates;

        this.listener = listener;
        this.longItemClickListener = longItemClickListener;
    }

    private static String convertNumberToString(Number value) {
        Locale locale = Locale.ENGLISH;
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        numberFormat.setMinimumFractionDigits(2); // trailing zeros
        numberFormat.setMaximumFractionDigits(3); // round to 2 digits

        return numberFormat.format(value);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder view, int position) {
        view.bind(position);
    }

    @Override
    public int getItemCount() {
        return modelCurrencies.size();
    }

    public interface LongClickListener {
        void longItemClick(RatesModel ratesModel);
    }

    public interface OnItemClickListener {
        void OnItemClick(RatesModel ratesModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_currency_image)
        ImageView imageView;
        @BindView(R.id.rv_currency_name)
        TextView currencyName;
        @BindView(R.id.rv_currency_description)
        TextView currencyDescription;
        @BindView(R.id.rv_rateCurrency)
        TextView rateCurrency;

        public ViewHolder(@NonNull View itemView)  {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
            imageView.setImageResource(R.drawable.ic_launcher_background);
            final RatesModel ratesModel = modelCurrencies.get(position);
            currencyName.setText(ratesModel.getCurrencyTitle());
            imageView.setImageResource(ratesModel.getCurrencyIcon());

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    if (longItemClickListener != null) {
                        longItemClickListener.longItemClick(ratesModel);
                        return true;
                    }
                    return false;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnItemClick(ratesModel);
                }
            });

            if (showRates) {
                rateCurrency.setVisibility(View.VISIBLE);
                String rate = convertNumberToString(ratesModel.getCurrencyRate());
                rateCurrency.setText(rate);
            } else rateCurrency.setVisibility(View.GONE);
        }
    }

}
